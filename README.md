Android PDF Writer

DESCRIPTION
___________

Android PDF Writer (APW) is a simple Java library to generate simple PDF documents in Google's Android devices released under the BSD license.

For more details, contact mezodevs@gmail.com.

Enjoy,
MEZOCLONE